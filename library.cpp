#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main()
{
  std::fstream file("books.txt");
  if(file.is_open()){
    string line;
    while(getline(file,line)){
      printf("%s",line.c_str());
    }
    file.close();
  }
  return 0;
}
